
**Thématique** :Réseaux et Internet

**Notions liées** :réseau informatique,Typologie de réseaux, BUS, ANNEAU, ÉTOILE

**Résumé de l’activité** :les élèves sont invités à donner des instructions dans l'interpréteur python.

**Objectifs** :Comprendre la notion de réseau informatique.

**Auteur** :Mohamed Darbali

**Durée de l’activité** :deux heures .

**Forme de participation** :Groupe de 5 personnes.

**Matériel nécessaire** :ordinateur et un retour est des cable RG45

**Préparation** :préparation du scénario pédagogique

**Fiche élève cours** :Disponible [ici](https://drive.google.com/file/d/17l8LNmDr75NUmiLPD-9fw5XDaLeyh0dN/view?usp=sharing)

**Fiche élève activité** :Disponible [ici](https://drive.google.com/file/d/1NGUiXBd1LWM4UWBTr8LGyF7oclAcOd7W/view?usp=sharing)
